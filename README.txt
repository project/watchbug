
This module has been developed to ease debugging by being able to look at any logged variable passed to the watchbug() function.
Especially when programming ajax reuquest or http requests this can easily fetch variables from processing, for having a delayed
closer look on them at the admin/watchbug - page.

You can call watchbug() also with alias wb(), and watchbug_clear() with alias wbc()

For further information go to drupal.org/project/watchbug