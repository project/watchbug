
/**
 * @file Javascript functions for watchbug_js.
 */

/**
 * Store a given value and print it via watchbug and its helper modules.
 *
 * @param value
 *   Value or js-object to store.
 * @param key
 *   Optional key to set for this value.
 */
function watchbug(value, key) {
  // AJAX call
  object_to_js(value);
  $.ajax({
    type: 'POST',
    url: Drupal.settings.basePath + 'admin/watchbug/js',
    dataType: 'json',
    data: 'value=' + object_to_js(value) + '&key=' + key,
    success: function() {}
  });
  return true;
}

/**
 * Converts a variable into its JSON equivalent. 
 *
 * Javascript implementation of drupal_to_js().
 * We use HTML-safe strings, i.e. with <, > and & escaped.
 *
 * @param object
 *   Variable to convert.
 */
function object_to_js(object) {
  if (typeof object === 'boolean') {
    return (object) ? 'true' : 'false'; // Lowercase necessary.
  }
  else if (typeof object === 'number') {
    return object;
  } 
  else if (typeof object === 'string') {
    return '"' + html_safe_string(addslashes(object)) + '"';
  }
  else if (typeof object === 'object') {
    var ret = '{ ';
    jQuery.each(object, function(key, value) {
      ret += object_to_js(new String(key) + ':' + object_to_js(value)) + ', ';
    });
    if (ret.length > 2){
      // Remove last ', ' if there is one.
      ret = ret.substring(0, ret.length-2);
    }
    return ret + ' }';
  }
  else {
    return 'null';    
  }
}

function html_safe_string(value) {
  value = value.replace(/\r/g,'\r');
  value = value.replace(/\n/g,'\n');
  value = value.replace(/</g,'\x3c');
  value = value.replace(/>/g,'\x3e');
  value = value.replace(/&/g,'\x26');
  return value;
}

function addslashes(str) {
  str = str.replace(/\\/g,'\\\\');
  str = str.replace(/\0/g,'\\0');
  str = str.replace(/\'/g,'\\\'');
  str = str.replace(/\"/g,'\\"');
  return str;
}
