<?php

/**
 * @file
 * Menu callbacks for watchbug 
 */

/**
 * Callback function for watchbug_menu().
 */
function watchbug_view_view($admin_links = FALSE) {
    $links = "";
    if ($admin_links) {
      $item = menu_get_item("admin/watchbug");
      if ($content = system_admin_menu_block($item)) {
        $links = "<br/><br/><hr/>" . theme('admin_block_content', $content);
      }
    }
    
    return watchbug_view(NULL, TRUE) ." $links";
}

/**
 * Clear the variable watchbug
 */
function watchbug_clear_view() {
  //for security reasons, watchbug_clear will only be fired by using a form
  $output = drupal_get_form('watchbug_clear_form');
  return $output;
}

/**
 * Form with submission button for watchbug_clear
 */
function watchbug_clear_form(&$form_state) {
  $form = array();
  $form['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Clear WatchBug'),
    '#prefix' => t("Click the button to clear the current watchbug variable.") ."<br/>",
  );
  return $form;
}

/**
 * submits watchbug clear, if button is pressed
 */
function watchbug_clear_form_submit($form, &$form_state) {  
  if ($form_state['submitted']  && $form_state['values']['op'] == t('Clear WatchBug')) {
    watchbug_clear();
    drupal_set_message(t('WatchBug cleared.'));
  }
}

/***
 * Shows the watchbug settings main form, that lists every module watchbug implements
 */
function watchbug_settings_form(&$form_state) {
  $form = array();
  //List active modules
  $modules = (array)module_implements("watchbug");
  $modules += (array)module_implements("watchbug_alter");
  $modules = array_unique($modules);
  if (count($modules)) {
    $form['active_modules'] = array(
      '#type' => 'fieldset',
      '#title' => t('Active modules'),
      '#collapsible' => TRUE,
      '#description' => t('a list of active watchbug modules'),
      '#suffix' => "<br/>",
    );
  }
  foreach ($modules as $module) {
    $info = drupal_parse_info_file(drupal_get_path('module', $module) .'/'. $module .'.info');
    $form['active_modules']['#children'] .= "<li>{$info['name']}</li>";
  }
  $form['active_modules']['#children'] = "<ul>". $form['active_modules']['#children'] ."</ul>"; 
  //Show links for additional settings pages
  $item = menu_get_item("admin/watchbug/settings");
  if ($content = system_admin_menu_block($item)) {
    $form['settings'] = array(
      '#prefix' => "<hr/>",
      '#value' => theme('admin_block_content', $content),
    );
  }
  $form = array("watchbug" => $form, '#tree' => TRUE);
  return $form; 
}